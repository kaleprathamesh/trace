package com.pratham.admin.trace.views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.pratham.admin.trace.R;
import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.utils.UserPrefManager;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ChildEventListener,Runnable {



    @BindView(R.id.map_distance_text)
    TextView mapDistanceText;
    private GoogleMap mMap;
    private User recipientUser;
    private User logginInUser;
    private Marker recpientMarker;
    private Marker loggedInMarker;
    Location receipientLocation = null;
    Location myLocation = null;
    private LatLng recipientLatLong;
    private LatLng loggedIntLatLong;
    private UserPrefManager prefManagerInstance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //logginInUser = UserPrefManager.getInstance(MapsActivity.this).getUser();
        prefManagerInstance = UserPrefManager.getInstance(MapsActivity.this);

        logginInUser = prefManagerInstance.getUser();


        checkIntent();
    }

    private void checkIntent() {
        Intent intent = getIntent();
        recipientUser = (User) intent.getSerializableExtra(Const.USER_INTENT_KEY);

        // add firebase listners on this
        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).addChildEventListener(this);
        //   FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).child(UserPrefManager.getInstance(MapsActivity.this).getUser().getPhone()).addChildEventListener(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


        // initialize latLng and location to calculate distance

        recipientLatLong = new LatLng(recipientUser.getLatitude(), recipientUser.getLongitude());
        receipientLocation = new Location("locationA");
        receipientLocation.setLatitude(recipientLatLong.latitude);
        receipientLocation.setLongitude(recipientLatLong.longitude);

        loggedIntLatLong = new LatLng(logginInUser.getLatitude(), logginInUser.getLongitude());
        myLocation = new Location("locationB");
        myLocation.setLatitude(loggedIntLatLong.latitude);
        myLocation.setLongitude(loggedIntLatLong.longitude);


        // set initoal marker
        setMarkerForUser(logginInUser);
        setMarkerForUser(recipientUser);


        // set Distance
        mapDistanceText.setText("Distance between you two is  " + ((receipientLocation.distanceTo(myLocation)) / 1000) + " KM");


    }

    private void setMarkerForUser(User user) {


        // LatLng loggedIntLatLong = null;
        //   LatLng recipientLatLong = null;

        // if (user.getPhone().equalsIgnoreCase(UserPrefManager.getInstance(MapsActivity.this).getUser().getPhone())) {
        if (user.getPhone().equalsIgnoreCase(prefManagerInstance.getUser().getPhone())) {
            loggedIntLatLong = new LatLng(user.getLatitude(), user.getLongitude());

            if (loggedInMarker != null) {
                loggedInMarker.remove();
            }
            loggedInMarker = mMap.addMarker(new MarkerOptions().position(loggedIntLatLong).title(user.getName()));

//            try {
//                setImageOnMarker();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }


            mMap.moveCamera(CameraUpdateFactory.newLatLng(loggedIntLatLong));

            myLocation.setLatitude(loggedIntLatLong.latitude);
            myLocation.setLongitude(loggedIntLatLong.longitude);


        } else {

            recipientLatLong = new LatLng(user.getLatitude(), user.getLongitude());

            if (recpientMarker != null) {
                recpientMarker.remove();
            }
            recpientMarker = mMap.addMarker(new MarkerOptions().position(recipientLatLong).title(user.getName()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(recipientLatLong));

            receipientLocation.setLatitude(recipientLatLong.latitude);
            receipientLocation.setLongitude(recipientLatLong.longitude);


        }


        mapDistanceText.setText("Distance between you two is  " + ((receipientLocation.distanceTo(myLocation)) / 1000) + " KM");


    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {


    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        User userInstance = dataSnapshot.getValue(User.class);
        // if (dataSnapshot.getKey().toString().equalsIgnoreCase(UserPrefManager.getInstance(MapsActivity.this).getUser().getPhone())) {
        if (dataSnapshot.getKey().toString().equalsIgnoreCase(prefManagerInstance.getUser().getPhone())) {
            logginInUser = userInstance;

            setMarkerForUser(logginInUser);
        } else {
            recipientUser = userInstance;

            setMarkerForUser(recipientUser);
        }

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        FirebaseDatabase.getInstance().getReference().child(recipientUser.getPhone()).removeEventListener(this);
        // FirebaseDatabase.getInstance().getReference().child(UserPrefManager.getInstance(MapsActivity.this).getUser().getPhone()).removeEventListener(this);

        FirebaseDatabase.getInstance().getReference().child(prefManagerInstance.getUser().getPhone()).removeEventListener(this);

    }

    public void setImageOnMarker() throws ExecutionException, InterruptedException {


       Thread custListLoadThread = new Thread(this);
        custListLoadThread.start();

    }

    @Override
    public void run() {

        final Bitmap theBitmap;

        try {
             theBitmap = Glide.
                    with(this).
                    load(UserPrefManager.getInstance(MapsActivity.this).getUser().getUrl()).
                    asBitmap().
                    into(100, 100). // Width and height
                    get();


             final Bitmap resizedbitmap1 = Bitmap.createBitmap(theBitmap, 0,0,100, 100);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    Bitmap.Config conf = Bitmap.Config.ARGB_8888;
//                //    Bitmap bmp = Bitmap.createBitmap(80, 80, conf);
//                    Canvas canvas1 = new Canvas(theBitmap);
//
//
//
//
//// paint defines the text color, stroke width and size
//                    Paint color = new Paint();
//                    color.setTextSize(35);
//                    color.setColor(Color.BLACK);



// modify canvas
                  //  canvas1.drawBitmap(resizedbitmap1, 0,0, color);
                   // canvas1.drawText("User Name!", 30, 40, color);

                    loggedInMarker = mMap.addMarker(new MarkerOptions().position(loggedIntLatLong).title(UserPrefManager.getInstance(MapsActivity.this).getUser().getName()));

                    loggedInMarker.setAnchor(0.5f, 1);
                    loggedInMarker.setIcon(BitmapDescriptorFactory.fromBitmap(resizedbitmap1));


                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }


    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }
}
