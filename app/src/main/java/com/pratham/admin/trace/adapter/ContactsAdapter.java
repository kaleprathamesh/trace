package com.pratham.admin.trace.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pratham.admin.trace.R;

import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.views.MapsActivity;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * Created by prathameshkale on 03/02/17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<User> array;
    private Context context;


    // Pass in the contact array into the constructor
    public ContactsAdapter(Context context, ArrayList<User> array) {
        this.array = array;
        this.context = context;

    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the custom layout

        View contactView = inflater.inflate(R.layout.list_item, parent, false);

        return new ViewHolderOne(contactView, context,array);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOne holderOne = (ViewHolderOne) holder;
        holderOne.nameText.setText(array.get(position).getName());
        holderOne.locationText.setText(""+array.get(position).getLatitude()+","+array.get(position).getLongitude());

        // reverse geocode
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {

            // at some weird location coordinates, this addreses array can come empty

                addresses = geocoder.getFromLocation(array.get(position).getLatitude(), array.get(position).getLongitude(), 1);
            if(!addresses.isEmpty()) {
                String feutureName = addresses.get(0).getFeatureName();
                String stateName = addresses.get(0).getAddressLine(1);

                String completeAddress = getCompleteAddressString(array.get(position).getLatitude(),array.get(position).getLongitude());
                holderOne.locationNameText.setText(completeAddress);

                // set updated Time
                if(array.get(position).getUpdatedTime()!=null) {

                    String s = DateUtils.getRelativeTimeSpanString(array.get(position).getUpdatedTime(), Calendar.getInstance().getTimeInMillis() ,DateUtils.MINUTE_IN_MILLIS).toString();
                    holderOne.updatedTime.setVisibility(View.VISIBLE);
                    holderOne.updatedTime.setText("" + s);
                }


                String countryName = addresses.get(0).getAddressLine(2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



        //  holderOne.nameText.setText(""+position);

    }

    @Override
    public int getItemCount() {
        return array.size();
    }


    public static class ViewHolderOne extends  RecyclerView.ViewHolder implements View.OnClickListener
    {

        public TextView nameText;
        public TextView locationText;
        public TextView locationNameText;
        public TextView updatedTime;
        private ArrayList<User> array;
        private  Context context;

        public ViewHolderOne(View itemView,Context context,ArrayList<User> array) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.list_item_name);
            locationText = (TextView) itemView.findViewById(R.id.list_item_location);
            locationNameText = (TextView) itemView.findViewById(R.id.list_item_location_name);
            updatedTime = (TextView) itemView.findViewById(R.id.updated_time);
            this.array = array;
            this.context = context;

            itemView.setOnClickListener(this);
        }



        public ViewHolderOne(View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.list_item_name);
            locationText = (TextView) itemView.findViewById(R.id.list_item_location);
            locationNameText = (TextView) itemView.findViewById(R.id.list_item_location_name);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            User user = array.get(position);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Const.USER_INTENT_KEY,user);
            Intent intent = new Intent(context, MapsActivity.class);
            intent.putExtras(bundle);

            context.startActivity(intent);


        }
    }

    public ArrayList<User> getArray() {
        return array;
    }

    public void setArray(ArrayList<User> array) {
        this.array = array;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    LATITUDE, LONGITUDE, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
                sb.append(address.getLocality()).append("\n");
                // sb.append(address.getPostalCode()).append("\n");
                sb.append(address.getCountryName());
                result = sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


}

