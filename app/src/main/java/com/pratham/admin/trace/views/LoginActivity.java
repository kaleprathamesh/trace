package com.pratham.admin.trace.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pratham.admin.trace.MainActivity;
import com.pratham.admin.trace.R;
import com.pratham.admin.trace.contract.LoginContract;
import com.pratham.admin.trace.presenter.LoginPresenter;
import com.pratham.admin.trace.service.LocationService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContract.view {

    public LoginPresenter presenterInstance;
    @BindView(R.id.login_email_edit)
   public EditText loginEmailEdit;
    @BindView(R.id.login_password_edit_text)
    EditText loginPasswordEditText;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.login_are_you_new_user)
    TextView loginAreYouNewUser;
    private LocationService locationService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenterInstance = new LoginPresenter(LoginActivity.this,this);


        // check if user is logged in
        if (isUserLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
            finish();
            startActivity(intent);
        }


        // dynamic permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermision();
        }


    }

    private void requestPermision() {

        ActivityCompat.requestPermissions(LoginActivity.this,
                new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay! Do the

        } else {
            requestPermision();
        }
    }

    @Override
    public void validateEmail() {

    }

    @Override
    public boolean isUserLoggedIn() {
        return presenterInstance.isUserLoggedIn();
    }


    public void fetchUserData(String loginEmail) {

        presenterInstance.fetchUserData(loginEmail);

    }

    @Override
    public void startLocationService() {

        // start the service
        locationService = new LocationService(LoginActivity.this);
        Intent intent = new Intent(LoginActivity.this, LocationService.class);
        startService(intent);
        Intent activityIntent = new Intent(LoginActivity.this, DrawerActivity.class);
        activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(activityIntent);

    }

    @Override
    public void showLoginFailedMessage(String message) {
        Toast.makeText(LoginActivity.this,message,Toast.LENGTH_LONG).show();

    }


    @OnClick({R.id.login_btn, R.id.login_are_you_new_user})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
            //    fetchUserData(loginEmailEdit.getText().toString());

                presenterInstance.loginWithFirebase(loginEmailEdit.getText().toString(),loginPasswordEditText.getText().toString());

                break;
            case R.id.login_are_you_new_user:
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }
}
