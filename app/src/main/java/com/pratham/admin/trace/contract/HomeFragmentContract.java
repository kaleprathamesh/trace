package com.pratham.admin.trace.contract;



import com.pratham.admin.trace.utils.User;

import java.util.ArrayList;

/**
 * Created by prathameshkale on 16/03/17.
 */

public interface HomeFragmentContract {

    void notifyChangedRecyclerView(ArrayList<User> array);

    void notifyItemChanged(User user, int position);


}
