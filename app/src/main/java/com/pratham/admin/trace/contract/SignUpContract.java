package com.pratham.admin.trace.contract;

/**
 * Created by admin on 3/13/2017.
 */

public interface SignUpContract {
    public interface View
    {
        void startService();

        void showErrorMessage(String message);
    }
}
