package com.pratham.admin.trace.contract;

/**
 * Created by admin on 3/12/2017.
 */

public interface LoginContract {

    public interface view
    {
        public void validateEmail();
        public boolean isUserLoggedIn();


        public void startLocationService();
        public void showLoginFailedMessage(String message);
    }
    public interface presenter
    {
        public void vlidateEmail();
        public boolean isUserLoggedIn();

    }
}
