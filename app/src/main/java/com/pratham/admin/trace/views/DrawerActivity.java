package com.pratham.admin.trace.views;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.auth.FirebaseAuth;
import com.pratham.admin.trace.R;
import com.pratham.admin.trace.service.LocationService;
import com.pratham.admin.trace.utils.CircularImageView;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.utils.UserPrefManager;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.nav_view)
    NavigationView navView;
    private FrameLayout frameLayout;
    private HomeFragment fragmentInstance;
    private LocationService locationService;

    private TextView nameText;
    private TextView emailText;
    private CircularImageView profileImage;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);

        frameLayout = (FrameLayout) findViewById(R.id.main_frame);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        View headerView = navView.getHeaderView(0);

        nameText = (TextView) headerView.findViewById(R.id.drawer_name_text);
        emailText = (TextView) headerView.findViewById(R.id.drawer_email_text);
        profileImage = (CircularImageView) headerView.findViewById(R.id.drawer_profile_image);

        // set the drawer
        nameText.setText(UserPrefManager.getInstance(this).getUser().getName());
        emailText.setText(UserPrefManager.getInstance(this).getUser().getEmail());

        if(UserPrefManager.getInstance(this).getUser().getUrl()!=null)
        {
            Picasso.with(this).load(UserPrefManager.getInstance(this).getUser().getUrl()).into(profileImage);
        }


        // start the service
        locationService = new LocationService(DrawerActivity.this);
        Intent intent = new Intent(DrawerActivity.this, LocationService.class);
        startService(intent);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                        .setMessage(getString(R.string.invitation_message))
                        .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                    //    .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                    //    .setCallToActionText(getString(R.string.invitation_cta))
                        .build();
                startActivityForResult(intent, 100);

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                User user = UserPrefManager.getInstance(DrawerActivity.this).getUser();
                if(UserPrefManager.getInstance(DrawerActivity.this).getUser().getUrl()!=null)
                {
                    Picasso.with(DrawerActivity.this).load(UserPrefManager.getInstance(DrawerActivity.this).getUser().getUrl())
                            .into(profileImage);
                }
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);

        if (fragmentInstance == null) {
            fragmentInstance = new HomeFragment();

        }






        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragmentInstance).commit();
    }

    @Override
    public void onBackPressed() {


        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_home) {


            // Handle the camera action
            getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragmentInstance).commit();
        } else if (id == R.id.menu_logout) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragmentInstance).commit();

            FirebaseAuth.getInstance().signOut();
            UserPrefManager.getInstance(DrawerActivity.this).logout();
            //TODO : sto the service
            locationService.stopSelf();

        } else if (id == R.id.menu_profile) {
            // Handle the camera action
            getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new ProfileFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(DrawerActivity.this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    LATITUDE, LONGITUDE, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
                sb.append(address.getLocality()).append("\n");
                // sb.append(address.getPostalCode()).append("\n");
                sb.append(address.getCountryName());
                result = sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("DrawerActivity", "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d("DrawerActivity", "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...


            }
        }
    }

}
