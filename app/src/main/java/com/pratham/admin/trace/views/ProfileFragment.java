package com.pratham.admin.trace.views;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pratham.admin.trace.R;
import com.pratham.admin.trace.contract.ProfileFragmentContract;
import com.pratham.admin.trace.model.User;
import com.pratham.admin.trace.presenter.ProfileFragmentPresenter;
import com.pratham.admin.trace.utils.CircularImageView;
import com.pratham.admin.trace.utils.UserPrefManager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Created by prathameshkale on 17/03/17.
 */

public class ProfileFragment extends Fragment implements ProfileFragmentContract{

    private static final int CAMERA_REQUEST = 100 ;
    private static final int CAMERA_REQUEST_CODE = 101 ;
    private static final int GALLARY_REQUEST_CODE = 102;
    @BindView(R.id.fragment_profile_image)
    CircularImageView fragmentProfileImage;
    @BindView(R.id.fragment_profile_camerabutton)
    ImageView fragmentProfileCamerabutton;
    @BindView(R.id.profile_name_edit)
    EditText profileNameEdit;
    @BindView(R.id.profile_name_text)
    TextView profileNameText;
    @BindView(R.id.fragment_profile_edit_name_image)
    ImageView fragmentProfileEditNameImage;
    @BindView(R.id.profile_phone_text)
    TextView profilePhoneText;
    @BindView(R.id.profile_email_text)
    TextView profileEmailText;
    @BindView(R.id.profile_current_location_text)
    TextView profileCurrentLocationText;
    @BindView(R.id.profile_last_updated_time)
    TextView profileLastUpdatedTime;

    public ProfileFragmentPresenter presenterInstance;
    private DrawerActivity activityInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, null);
        ButterKnife.bind(this, v);
        presenterInstance = new ProfileFragmentPresenter(this);
        presenterInstance.fetchUserProfile(UserPrefManager.getInstance(getActivity()));
        activityInstance = (DrawerActivity) getActivity();
        return v;
    }

    @OnClick({R.id.fragment_profile_camerabutton, R.id.fragment_profile_edit_name_image})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_profile_camerabutton:


                if (checkSelfPermission(getActivity(),Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST);
                }
                else if (checkSelfPermission(getActivity(),Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED){
//                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);

                    generateAlertWithAttachementOptions();
                }

                break;
            case R.id.fragment_profile_edit_name_image:
                break;
        }
    }

    @Override
    public void showMessage(String message) {

        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();

    }

    @Override
    public void updateProfile(User user) {

        profilePhoneText.setText(user.getPhone());
        profileEmailText.setText(user.getEmail());
        profileNameText.setText(user.getName());

        profileCurrentLocationText.setText(activityInstance.getCompleteAddressString(user.getLatitude(),user.getLongitude()));

        if(user.getUrl()!=null)
        {
            Picasso.with(getActivity()).load(user.getUrl()).
                    into(fragmentProfileImage);
        }

    }

    @Override
    public void showImageAfterUploading() {

        Log.d("ProfileFragment", "showImageAfterUploading: "+UserPrefManager.getInstance(getActivity()).getUser().getUrl());

        Picasso.with(getActivity()).load(UserPrefManager.getInstance(getActivity()).getUser().getUrl()).
                into(fragmentProfileImage);

    }


    /*Select options*/
    void generateAlertWithAttachementOptions()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Attach From:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1);
        arrayAdapter.add("CAMERA");
        arrayAdapter.add("GALLERY");


        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which==0)
                        {
                            //To check the permission for the camera
                            int permissionCheck = ContextCompat.checkSelfPermission(getContext(),
                                    Manifest.permission.CAMERA);
                            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {

                                Toast.makeText(ProfileFragment.super.getContext(), "Please allow permission for Camera from settings", Toast.LENGTH_SHORT).show();

                            }
                            else {

                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
                            }

                        }

                        else if (which==1)
                        {
                            Intent galaryIntent = new Intent();
                            // Show only images, no videos or anything else
                            galaryIntent.setType("image/*");
                            galaryIntent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(galaryIntent, GALLARY_REQUEST_CODE);
                        }
                    }
                });
        builderSingle.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        super.onActivityResult(requestCode, resultCode, intentData);
        // On Activity Result method for the image capture
        if(requestCode==CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK)
        {

            Bitmap photoBitMap = (Bitmap) intentData.getExtras().get("data");

            presenterInstance.uploadImageFromCamera(UserPrefManager.getInstance(getActivity()),photoBitMap);

        }

        else {
            if (requestCode == GALLARY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                Bitmap bm = null;
                if (intentData != null) {
                    //........
                    try {


                        Uri selectedImage = intentData.getData();
                        bm = decodeUri(selectedImage);//MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), intentData.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);//100
                    byte[] data = baos.toByteArray();

                    presenterInstance.uploadImageFromGallery(intentData,UserPrefManager.getInstance(getActivity()),data);


                //    attorneyImageLayout.setImageBitmap(bm);

                //    ByteArrayInputStream bis = new ByteArrayInputStream(data);
                    //............

                }


            }
        }
    }


    /*Sampling the image*/
    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        Bitmap bm =  BitmapFactory.decodeStream(
                getActivity().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 500;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;//calculateInSampleSize(o2,100,100);



        o2.inScaled = false;
        return BitmapFactory.decodeStream(
                getActivity().getContentResolver().openInputStream(selectedImage), null, o2);
    }

}
