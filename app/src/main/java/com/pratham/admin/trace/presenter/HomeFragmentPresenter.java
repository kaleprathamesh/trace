package com.pratham.admin.trace.presenter;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pratham.admin.trace.contract.HomeFragmentContract;

import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.User;

import java.util.ArrayList;

/**
 * Created by prathameshkale on 16/03/17.
 */

public class HomeFragmentPresenter implements ChildEventListener{

    private ArrayList<User> userArray;
    private HomeFragmentContract contractInstance;

    public HomeFragmentPresenter() {

    }

    public HomeFragmentPresenter(HomeFragmentContract contractInstance) {
        this.contractInstance = contractInstance;
        userArray = new ArrayList<>();
    }

    public void fetchLocationsApi() {

        userArray.clear();

        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).addChildEventListener(this);



        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              //  adapter.notifyDataSetChanged();
                contractInstance.notifyChangedRecyclerView(userArray);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }





    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

        User user = dataSnapshot.getValue(User.class);
        userArray.add(user);

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        User user = dataSnapshot.getValue(User.class);
        Log.d("MainActivity", "onChildChanged: "+s);
        Log.d("MainActivity", "onChildChanged: "+s);
        for(int i=0; i<userArray.size();i++)
        {
            if(userArray.get(i).getPhone().equalsIgnoreCase(user.getPhone()))
            {

                userArray.set(i,user);
                contractInstance.notifyChangedRecyclerView(userArray);
              //  adapter.notifyItemChanged(i);
            }
        }




    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public void removeCallbacks()
    {
        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).removeEventListener(this);
    }
}
