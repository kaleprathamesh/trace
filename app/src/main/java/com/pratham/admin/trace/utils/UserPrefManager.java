package com.pratham.admin.trace.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.pratham.admin.trace.views.LoginActivity;


/**
 * Created by prathameshkale on 18/01/17.
 */




public class UserPrefManager {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    static UserPrefManager prefManagerInstance;
    private Context context;


    public UserPrefManager(Context context)
    {
        sharedPref = context.getSharedPreferences(Const.USER_SHARE_PREF_KEY, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        this.context = context;

    }


    public static UserPrefManager getInstance(Context context)
    {
        if(prefManagerInstance == null)
        {
            prefManagerInstance = new UserPrefManager(context);
        }

      return  prefManagerInstance;
    }


    public User getUser()
    {
        User user = new User();
        user.setName(sharedPref.getString(Const.PREF_USER_NAME,null));
        user.setPhone(sharedPref.getString(Const.PREF_USER_PHONE,null));
        user.setEmail(sharedPref.getString(Const.PREF_USER_EMAIL,null));
        user.setLatitude(Double.parseDouble(sharedPref.getString(Const.PREF_USER_LATITUDE,null)));
        user.setLongitude(Double.parseDouble(sharedPref.getString(Const.PREF_USER_LONGITUDE,null)));
        user.setUrl(sharedPref.getString(Const.PREF_USER_PROFILE_URL,null));
        user.setUpdatedTime(sharedPref.getLong(Const.PREF_USER_UPDATED_TIME,0));


        return user;
    }

    public void insertUserInLocal(User user)
    {
        editor.putString(Const.PREF_USER_NAME,user.getName());
        editor.putString(Const.PREF_USER_PHONE,user.getPhone());
        editor.putString(Const.PREF_USER_EMAIL,user.getEmail());
        editor.putString(Const.PREF_USER_LATITUDE,String.valueOf(user.getLatitude()));
        editor.putString(Const.PREF_USER_LONGITUDE,String.valueOf(user.getLongitude()));
        editor.putString(Const.PREF_USER_PROFILE_URL,String.valueOf(user.getUrl()));
        editor.putLong(Const.PREF_USER_UPDATED_TIME,user.getUpdatedTime());



        editor.putBoolean(Const.PREF_IS_USER_LOGGED_IN,true);
        editor.commit();
    }

    public void logout()
    {
        editor.clear();
        editor.commit();
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public boolean isUserLoggedIn()
    {
        return sharedPref.getBoolean(Const.PREF_IS_USER_LOGGED_IN, false);
    }


    public UserPrefManager getUserPreference()
    {
        return this;
    }
}
