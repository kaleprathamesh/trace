package com.pratham.admin.trace.views;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pratham.admin.trace.R;
import com.pratham.admin.trace.adapter.ContactsAdapter;
import com.pratham.admin.trace.contract.HomeFragmentContract;

import com.pratham.admin.trace.presenter.HomeFragmentPresenter;
import com.pratham.admin.trace.service.LocationService;
import com.pratham.admin.trace.utils.User;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by prathameshkale on 15/03/17.
 */

public class HomeFragment extends Fragment implements HomeFragmentContract{
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private ArrayList<User> userArray;
    private ContactsAdapter adapter;
    private HomeFragmentPresenter presenterInstance;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d("HomeFragment", "onCreateView: ");

        View v = inflater.inflate(R.layout.fragment_home, null);

        presenterInstance = new HomeFragmentPresenter(this);



        ButterKnife.bind(this, v);

        userArray = new ArrayList<>();
        adapter = new ContactsAdapter(getActivity(), userArray);
       // contactArray = new ArrayList<>();




       // contactArray.add("9822354292");

        LinearLayoutManager layoutmanager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.setAdapter(adapter);

        /* Firebase RecyclerAdapter*/

     //   FirebaseRecyclerAdapter<User, ContactsAdapter.ViewHolderOne> adapter;
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();//.child(Const.LOCATIONS).getDatabase().getReference();


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenterInstance.fetchLocationsApi();



        return v;
    }


    @Override
    public void notifyChangedRecyclerView(ArrayList<User> array) {

        userArray.clear();
        userArray.addAll(array);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void notifyItemChanged(User user, int position) {
        userArray.set(position,user);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterInstance.removeCallbacks();
    }


}
