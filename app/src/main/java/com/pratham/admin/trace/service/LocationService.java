package com.pratham.admin.trace.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.utils.UserPrefManager;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import static android.content.ContentValues.TAG;


public class LocationService extends Service implements LocationListener {

    private Context context;


    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 500; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 0; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;


    UserPrefManager prefManagerInstance;
   // private MyApplication myApplication;


    public LocationService() {

    }


    public LocationService(Context context) {
        this.context = context;



    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand: ");

        prefManagerInstance = UserPrefManager.getInstance(getApplicationContext());

     //   myApplication = (MyApplication) getApplication();
      //  myApplication.getComponent().inject(this);


        fetchLocation();
        return START_STICKY;

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onLocationChanged(Location location) {

        this.location = location;

        Log.d(TAG, "onLocationChanged: Latitude "+ location.getLatitude());
        Toast.makeText(getApplicationContext(),"Location Changed in my own location", Toast.LENGTH_SHORT).show();




        //upload location
        uploadLocation();


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

        Log.d(TAG, "onProviderEnabled: Latitude ");

        Toast.makeText(getApplicationContext(),"location On",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onProviderDisabled(String s) {

        Log.d(TAG, "onProviderDisabled: Latitude ");
        Toast.makeText(getApplicationContext(),"location Off",Toast.LENGTH_LONG).show();
    }


    /**/

    public Location fetchLocation() {

        Log.d(TAG, "fetchLocation: fetch location method called");

        try {
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

            // first request location updates from both the providers
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {



                        // permision not granted
                       // return 0;
                    }

                   // resetting updates
                    locationManager.removeUpdates(this);
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }


            }

        } catch (Exception e) {
            e.printStackTrace();

           // stopSelf();
        }
        return location;
    }


    void uploadLocation()
    {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();

      //  final User localUser = UserPrefManager.getInstance(getApplicationContext()).getUser();
        final User localUser = prefManagerInstance.getUser();


        Log.d(TAG, "uploadLocation: upload location gets called ");
        
        if(location!=null) {

            fbDatabase.getReference().child(Const.LOCATIONS).
                    child(localUser.getPhone()).child(Const.LATITUDE).setValue(location.getLatitude());

            fbDatabase.getReference().child(Const.LOCATIONS).
                    child(localUser.getPhone()).child(Const.UPDATED_TIME).setValue(Calendar.getInstance().getTimeInMillis());

            fbDatabase.getReference().child(Const.LOCATIONS).
                   // child(UserPrefManager.getInstance(getApplicationContext()).getUser().getPhone()).child(Const.LONGITUDE).setValue(location.getLongitude()).addOnCompleteListener(new OnCompleteListener<Void>() {
                           child(prefManagerInstance.getUser().getPhone()).child(Const.LONGITUDE).setValue(location.getLongitude()).addOnCompleteListener(new OnCompleteListener<Void>() {

                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful())
                    {
                        Log.d(TAG, "uploadLocation: uploaded location succesfully ");
                    }
                    else
                    {
                        Log.d(TAG, "uploadLocation: uploaded location failed "+task.getException().toString());
                    }

                    // update the location in local

                    localUser.setLatitude(location.getLatitude());
                    localUser.setLongitude(location.getLongitude());
//                    UserPrefManager.getInstance(getApplicationContext()).insertUserInLocal(localUser);
                    prefManagerInstance.insertUserInLocal(localUser);

                  // long time= 1490034600000L;
                    String s = DateUtils.getRelativeTimeSpanString(1490034600000L, Calendar.getInstance().getTimeInMillis() ,DateUtils.MINUTE_IN_MILLIS).toString();
                  //  Log.d("Time", "onComplete: "+s);




                }
            });
        }
       // fbDatabase.getReference().child(Const.LOCATIONS).child(user.getEmail()).child(Const.LONGITUDE).setValue(location.getLongitude());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        Log.d(TAG, "onTaskRemoved: ");
        Toast.makeText(getApplicationContext(),"location app  Destroyed",Toast.LENGTH_LONG).show();

        fetchLocation();
//        Intent intent = new Intent(getApplicationContext(),LocationService.class);
//        startService(intent);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        Toast.makeText(getApplicationContext(),"Service Destroyed",Toast.LENGTH_LONG).show();
    }
}
