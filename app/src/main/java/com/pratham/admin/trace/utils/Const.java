package com.pratham.admin.trace.utils;

/**
 * Created by prathameshkale on 02/02/17.
 */

public class Const {

    public static final String LOCATIONS = "locations";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String UPDATED_TIME = "updatedTime";



    public static final String USER_SHARE_PREF_KEY = "userSharePrefKey";

    public static final  String  PREF_USER_NAME = "name";
    public static final  String  PREF_USER_PHONE = "phone";
    public static final  String  PREF_USER_EMAIL = "email";
    public static final  String  PREF_USER_LATITUDE = "latitude";
    public static final  String  PREF_USER_LONGITUDE = "longitude";
    public static final  String  PREF_USER_PROFILE_URL = "url";
    public static final  String  PREF_USER_UPDATED_TIME = "updatedTime";


    public static final  String  PREF_USER_AUTH_STRING = "auth_string";
    public static final  String  PREF_IS_USER_LOGGED_IN= "isLoggedIn";


    public static final  String  USER_INTENT_KEY= "userInstance";

    public static final String STORAGE_TRACE ="TraceStorage" ;
    public static final String STORAGE_PROFILE_IMAGES ="ProfileImages" ;
    public static final String URL ="url" ;


}
