package com.pratham.admin.trace.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pratham.admin.trace.contract.SignUpContract;
import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.User;

import static com.google.android.gms.internal.zzt.TAG;

/**
 * Created by admin on 3/13/2017.
 */

public class SignUpPresenter implements ChildEventListener{

    public SignUpContract.View view;
    public FirebaseAuth firebaseAuth;
    public User user;
    public String password;


    public SignUpPresenter(SignUpContract.View view) {
        this.view = view;
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void signUpInFirebase(final User user, String password)
    {
        this.user = user;
        this.password = password;

        firebaseAuth.createUserWithEmailAndPassword(user.getEmail().toString(),password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {

                if(task.isSuccessful())
                {

                    checkThisMobileNumberAlreadyExists();


                }
                else
                {
                    view.showErrorMessage("Sign up failed");
                }

            }
        });




    }

    private void checkThisMobileNumberAlreadyExists() {



    //    FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).addChildEventListener(this);

        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild(user.getPhone()))
                {
                    deleteTheUser();
                }
                else
                {
                    FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).
                    child(user.getPhone().toString()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    view.startService();



                }
            });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

        if(dataSnapshot.getKey().equalsIgnoreCase(user.getPhone()))
        {



        }
        else
        {

            // set data in firebaseuser and then in local


        }

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {



    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

        view.showErrorMessage(databaseError.getMessage());

    }

    public void removeALlListners()
    {
       // FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).removeEventListener(this);
    }


    public void deleteTheUser()
    {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Get auth credentials from the user for re-authentication. The example below shows
        // email and password credentials but there are multiple possible providers,
        // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider
                .getCredential(user.getEmail(), password);

        // Prompt the user to re-provide their sign-in credentials
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        user.delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d("SignUpPresenter", "User account deleted.");

                                            view.showErrorMessage("Sorry! This phone number is already registered. Please try again with new phone number");
                                        }
                                    }
                                });

                    }
                });
    }
}


