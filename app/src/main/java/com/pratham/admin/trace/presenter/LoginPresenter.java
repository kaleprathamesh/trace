package com.pratham.admin.trace.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pratham.admin.trace.MainActivity;
import com.pratham.admin.trace.contract.LoginContract;
import com.pratham.admin.trace.service.LocationService;
import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.Network;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.utils.UserPrefManager;
import com.pratham.admin.trace.views.LoginActivity;

import java.util.Iterator;

/**
 * Created by admin on 3/12/2017.
 */

public class LoginPresenter implements LoginContract.presenter {
    public Context context;
    public UserPrefManager prefManagerInstance;
    private LocationService locationService;
    private LoginContract.view view;

    public LoginPresenter(Context context,LoginContract.view view) {
        this.context = context;
        prefManagerInstance = UserPrefManager.getInstance(context);
        this.view = view;
    }

    @Override
    public void vlidateEmail() {

    }

    @Override
    public boolean isUserLoggedIn() {


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            //if(UserPrefManager.getInstance(LoginActivity.this).isUserLoggedIn())
            if(prefManagerInstance.isUserLoggedIn())
            {
                return  true;
            }
        }

        return false;
    }

    public void  loginWithFirebase(final String emailText, String passwordText)
    {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(emailText.toString(), passwordText.toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {


                if (!task.isSuccessful()) {
                    // there was an error
                    view.showLoginFailedMessage("login failed");


                } else {

                    fetchUserData(emailText);




                }
            }
        });
    }


    public void fetchUserData(final String userName) {

        Network.fetchSingleValue(FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).getRef(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                while (iterator.hasNext())
                {
                    DataSnapshot innerSnapshot = iterator.next();

                    if(innerSnapshot.child(Const.PREF_USER_EMAIL).getValue().toString().
                            equalsIgnoreCase(userName))
                    {
                        Log.d("LoginActivity", "onDataChange: login data found");
                        User user = innerSnapshot.getValue(User.class);
                        Log.d("LoginActivity", "onDataChange: user data found"+ user.getPhone());
                        //  UserPrefManager.getInstance(LoginActivity.this).insertUserInLocal(user);
                        prefManagerInstance.insertUserInLocal(user);

                        view.startLocationService();
                        
                        break;
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                view.showLoginFailedMessage("login failed");

            }
        });

    }
}
