package com.pratham.admin.trace.contract;

import com.pratham.admin.trace.model.User;

/**
 * Created by admin on 3/19/2017.
 */

public interface ProfileFragmentContract {

    public void showMessage(String meassage);

    public void updateProfile(User user);

    public void showImageAfterUploading();

}


