package com.pratham.admin.trace.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.pratham.admin.trace.MainActivity;
import com.pratham.admin.trace.R;
import com.pratham.admin.trace.contract.SignUpContract;
import com.pratham.admin.trace.presenter.SignUpPresenter;
import com.pratham.admin.trace.service.LocationService;
import com.pratham.admin.trace.utils.User;
import com.pratham.admin.trace.utils.UserPrefManager;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {

    @BindView(R.id.signup_name_edit)
    EditText signupNameEdit;
    @BindView(R.id.sign_up_email_edit)
    EditText signUpEmailEdit;
    @BindView(R.id.sign_up_phone)
    EditText signUpPhone;
    @BindView(R.id.sign_up_password)
    EditText signUpPassword;
    @BindView(R.id.sign_up_submit_btn)
    Button signUpSubmitBtn;

    public User user;
    public SignUpPresenter presenterInstance;
    private LocationService locationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        presenterInstance = new SignUpPresenter(SignUpActivity.this);
    }

    @OnClick(R.id.sign_up_submit_btn)
    public void onClick() {
         user = new User();
        user.setEmail(signUpEmailEdit.getText().toString());
        user.setName(signupNameEdit.getText().toString());
        user.setPhone(signUpPhone.getText().toString());
        user.setUpdatedTime(Calendar.getInstance().getTimeInMillis());


        presenterInstance.signUpInFirebase(user,signUpPassword.getText().toString());
    }

    @Override
    public void startService() {

        user.setUuid(FirebaseAuth.getInstance().getCurrentUser().getUid());

           UserPrefManager.getInstance(SignUpActivity.this).insertUserInLocal(user);



                            locationService = new LocationService(SignUpActivity.this);
                            Intent intent = new Intent(SignUpActivity.this, LocationService.class);
                            startService(intent);

                            Intent activityIntent = new Intent(SignUpActivity.this, DrawerActivity.class);
                    activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                      startActivity(activityIntent);
                            finish();

    }

    @Override
    public void showErrorMessage(String message) {

        Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenterInstance.removeALlListners();
    }
}
