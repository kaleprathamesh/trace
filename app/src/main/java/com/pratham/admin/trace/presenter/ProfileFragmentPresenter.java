package com.pratham.admin.trace.presenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pratham.admin.trace.contract.ProfileFragmentContract;

import com.pratham.admin.trace.model.User;
import com.pratham.admin.trace.utils.Const;
import com.pratham.admin.trace.utils.Network;

import com.pratham.admin.trace.utils.UserPrefManager;
import com.pratham.admin.trace.views.ProfileFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Iterator;

/**
 * Created by admin on 3/19/2017.
 */

public class ProfileFragmentPresenter implements ChildEventListener {

    public ProfileFragmentContract contractInstance;

    public ProfileFragmentPresenter(ProfileFragmentContract contractInstance) {
        this.contractInstance = contractInstance;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }


    public void fetchUserProfile(UserPrefManager userPref)
    {
//        FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).child(userPref.getUser().getPhone())
//                .addChildEventListener(this);

        Network.fetchSingleValue(FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).child(userPref.getUser().getPhone()).getRef(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {




                        Log.d("LoginActivity", "onDataChange: login data found");
                        User user = dataSnapshot.getValue(User.class);
                    contractInstance.updateProfile(user);




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

             //   view.showLoginFailedMessage("login failed");
                contractInstance.showMessage("Error occured");

            }
        });
    }


    public void uploadImageFromCamera(final UserPrefManager userPref, Bitmap bm)
    {
        StorageReference attachementRef = FirebaseStorage.getInstance().getReference().child(Const.STORAGE_TRACE).
                child(Const.STORAGE_PROFILE_IMAGES).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("IMG_"+userPref.getUser().getName());

       // Bitmap photoBitMap = (Bitmap) intentData.getExtras().get("data");


      //  attorneyImageLayout.setImageBitmap(photoBitMap);



        // upload data in firebase
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();



        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpg")
                .setCacheControl("public,max-age=300")
                .build();

        UploadTask uploadTask = attachementRef.putBytes(data);

        attachementRef.updateMetadata(metadata)
                .addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        // Updated metadata is in storageMetadata
                        Log.d("AttorneyEditProment", "onSuccess: success");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                        Log.d("AttorneyEditProment", "onSuccess: false");
                    }
                });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                // Toast.makeText(ClientAttachmentGridActivity.this,"uploading failed",Toast.LENGTH_LONG).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                //  Toast.makeText(ClientAttachmentGridActivity.this,"successfuly uploaded",Toast.LENGTH_LONG).show();
                Log.d("AttachmentGridActivity", "download succeess: "+downloadUrl.toString());


                // add image url to local storage

                com.pratham.admin.trace.utils.User user = userPref.getUser();
                user.setUrl(downloadUrl.toString());
                userPref.insertUserInLocal(user);


                // insert in firebase
                FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).child(userPref.getUser().getPhone())
                        .child(Const.URL).setValue(downloadUrl.toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        contractInstance.showImageAfterUploading();

                    }
                });


            }
        });
    }


    public void uploadImageFromGallery(Intent intentData, final UserPrefManager userPref,byte[] data)
    {

        final File file = new File(intentData.getData().getPath());

        StorageReference attachementRef = FirebaseStorage.getInstance().getReference().child(Const.STORAGE_TRACE).
                child(Const.STORAGE_PROFILE_IMAGES).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("IMG_"+userPref.getUser().getName());

        // UploadTask uploadTask = attachementRef.putFile(intentData.getData());
        UploadTask uploadTask = attachementRef.putBytes(data);//Uri.fromFile(file) //data
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                contractInstance.showMessage("uploading failed");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                //  Toast.makeText(ClientAttachmentGridActivity.this,"successfuly uploaded",Toast.LENGTH_LONG).show();
                Log.d("AttachmentGridActivity", "download succeess: "+downloadUrl.toString());


                // add image url to local storage

                com.pratham.admin.trace.utils.User user = userPref.getUser();
                user.setUrl(downloadUrl.toString());
                userPref.insertUserInLocal(user);


                // insert in firebase
                FirebaseDatabase.getInstance().getReference().child(Const.LOCATIONS).child(userPref.getUser().getPhone())
                        .child(Const.URL).setValue(downloadUrl.toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        contractInstance.showImageAfterUploading();

                    }
                });
            }
        });
    }

}
